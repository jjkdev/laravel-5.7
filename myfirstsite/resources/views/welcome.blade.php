@extends('layouts.homepage')

{{-- @section('title')
  jjkdev
@endsection --}}

@section('content')
  <h1>It's my first Laravel webpage!</h1>

  <ul>
    @foreach( $tasks as $task )
      <li>
        {{-- To display escaped data: --}}
        {{ $task }}
        {{-- To display unescaped data: --}}
        {{-- {!! $task !!} --}}
      </li>
    @endforeach
  </ul>

@endsection
